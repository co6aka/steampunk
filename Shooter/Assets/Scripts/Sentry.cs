﻿using UnityEngine;

public class Sentry : MonoBehaviour
{
    public float moveSpeed = 0.2f;
    public GameObject[] waypoints;

    private Vector3 distanceToNextWp;
    //on which distance you want to switch to the next waypoint
    public float changeDirDistance = 1f;
    public float rotateToNextWpDist = 1f;   // when we should start facing next WP
    private Rigidbody rb;
    private Attacker shooter;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        shooter = GetComponentInChildren<Attacker>();
    }

    void FixedUpdate()
    {
        // travel between waypoints, patroling the area
        // if a player is sensed, stop, aim at the player and start shooting
    }
}
