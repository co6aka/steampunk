﻿using UnityEngine;

// consume health / ammo packs
public class Consumer : MonoBehaviour
{
    Shooter player;
    public string type = "ammo";

    void Start()
    {
        player = GameObject.Find("Player").GetComponentInChildren<Shooter>();
    }

    private void OnMouseEnter()
    {
        Pointer.SetAction("explore");
    }

    private void OnMouseExit()
    {
        Pointer.SetAction("aim");
    }

    private void OnMouseUp()
    {       
        if (Pointer.state != "explore")
            return;

        if (type == "ammo")
            // add ammo to the player
            ;
        else
            // add health to the player
            ;

        Invoke("ResetAction", 0.2f);  // so we don't shoot as well
    }
   
    void ResetAction()
    {
        Pointer.SetAction("aim");
        Destroy(gameObject);
    }
}
