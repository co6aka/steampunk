﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Barrel : MonoBehaviour
{
    public GameObject fireFx, explosionFx;
    public AudioClip sound;

    AudioSource audioPlay;
    bool hit = false;
    GameObject player;

    void Start()
    {
        audioPlay = gameObject.AddComponent<AudioSource>();
        player = GameObject.Find("Player");
    }

    private void OnCollisionEnter(Collision collision)
    {
        // print("barrel collided with " + collision.gameObject.name);
        if (hit == true)
            return;

        if(collision.gameObject.tag == "PlayerBullet")
        {
            hit = true;
            Vector3 firePos = new Vector3(transform.position.x, transform.position.y + 0.3f, transform.position.z);
            GameObject flame = Instantiate(fireFx, firePos, Quaternion.identity);
            flame.transform.Rotate(-90f, 0f, 0f);  // go up
            Invoke("Explode", 3.7f);
        }
    }

    void Explode()
    {
        Instantiate(explosionFx, transform.position, Quaternion.identity);
        if (sound != null)
            AudioSource.PlayClipAtPoint(sound, player.transform.position);  // make it loud?
        MeshRenderer mr = GetComponentInParent<MeshRenderer>();
        Destroy(mr);
        // TODO: destroy any Scout orbs in the area
        Destroy(gameObject, 1f);
    }
}
