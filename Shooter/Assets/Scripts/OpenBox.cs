﻿using System.Collections;
using UnityEngine;

public class OpenBox : MonoBehaviour
{
    bool opened = false;

    // note: if you have CharacterController in a Player, don't add Rigidbody to it
    // or these Mouse() functions (and possibly Raycasts) will be messed up
    private void OnMouseEnter()
    {
        print("OnMouseEnter");
        if(Pointer.state != "explore")  // avoid cursor jittering while over the object
          Pointer.SetAction("explore");
    }

    private void OnMouseExit()
    {
        if (Pointer.state != "aim")
            Pointer.SetAction("aim");
    }

    private void OnMouseDown()
    {
        if (Pointer.state != "explore")
            return;

        if(! opened)
        {
            opened = true;
            StartCoroutine(Open());
        }
    }

    IEnumerator Open()
    {
        for(int i=0;i<30; i++)
        {
            transform.RotateAround(transform.position, transform.right, -1f);
            yield return null;  // new WaitForSeconds(0.01f);
        }
        Pointer.SetAction("aim");
        Destroy(gameObject);
    }
}
