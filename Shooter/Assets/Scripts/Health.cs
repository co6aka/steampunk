﻿using UnityEngine;
using System.Collections;

public class Health : MonoBehaviour
{
    public GameObject damage, explosion;
    public int HP=8;
    bool dead = false;
    public AudioClip expSound;

    AudioSource player;
    
    void Explode()
    {
        // play explosive audio
        if (explosion != null)
            Instantiate(explosion, transform.position, Quaternion.identity);
        if (expSound != null)
            AudioSource.PlayClipAtPoint(expSound, transform.position);

        MeshRenderer mr = GetComponentInParent<MeshRenderer>();
        Destroy(mr);

        // give 'em some fireworks
        Invoke("PlayDamage", 0.1f);
        Invoke("PlayDamage", 0.2f);
        Invoke("PlayDamage", 0.3f);
        Invoke("PlayDamage", 0.4f);
        Destroy(gameObject, 0.5f);
    }

     void PlayDamage()
    {
        Instantiate(damage, transform.position, Quaternion.identity);  // rotate it facing the player for nice effect
    }
}
