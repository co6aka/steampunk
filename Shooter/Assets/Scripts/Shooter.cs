﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Shooter : MonoBehaviour
{
    public GameObject bullet;
    public Text ammoLbl, healthLbl;
    public Transform muzzle;
    private Animator m_Animator;
    int health = 60;
    int ammo = 28;

    int maxHealth = 100;
    int maxAmmo = 100;

    void Start()
    {
        m_Animator = GetComponent<Animator>();
    }

   
    void Update()
    {
        if (Pointer.state != "aim")  // exploring, not shooting
            return;

        // on left mouse click, fire a single shot, on right click, fire a salvo
        // ...

        // reload
        if (Input.GetKeyDown(KeyCode.R)) {
            m_Animator.Play("Automatic_Rifle_Clip_Eject", 0, 0.0f);
            Invoke("LoadClip", 2f);
        }
    }

    void SingleShot()
    {
        if (ammo == 0)
            return;

        m_Animator.Play("Automatic_Rifle_Single_Shot", 0, 0.0f);
        GameObject shot = Instantiate(bullet, muzzle.transform.position, transform.rotation);
        shot.GetComponent<Rigidbody>().AddForce(transform.forward);
        ammo--;
        // update anmo label..
    }

   

    void LoadClip()
    {
            m_Animator.Play("Automatic_Rifle_Clip_Insert", 0, 0.0f);
    }

}
