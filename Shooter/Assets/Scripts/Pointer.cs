﻿using UnityEngine;

public class Pointer : MonoBehaviour
{
    public Texture2D hand;
    public GameObject crossHairs;
    Vector2 hotSpot;
    public static string state = "aim";
    public static Pointer me;

    private void Awake()
    {
        me = this;
    }

    public static void SetAction(string action)
    {
        if (state == action)
            return;

        state = action;

        if (action == "aim")
        {
       //     print("aiming");
            Cursor.lockState = CursorLockMode.Locked;  // invisible as well
            Cursor.SetCursor(null, Vector2.zero, CursorMode.Auto);  // set default pointer
            me.crossHairs.SetActive(true);

        }
        else
        {
     //       print("exploring");
            Cursor.lockState = CursorLockMode.None;
            me.crossHairs.SetActive(false);
            me.SetIcon(me.hand, 0f, 0f);
            
            Cursor.visible = true;            
        }
    }

    void SetIcon(Texture2D crs, float xHotspot=0.5f, float yHotspot=0.5f)
    {        
        hotSpot = new Vector2(crs.width * xHotspot, crs.height * yHotspot);  // corsshairs, with hotspot in the middle
        Cursor.SetCursor(crs, hotSpot, CursorMode.Auto);    
    }
    /*
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.E))
            SetAction("explore");
        if (Input.GetKeyDown(KeyCode.A))
            SetAction("aim");
    }
    */
}