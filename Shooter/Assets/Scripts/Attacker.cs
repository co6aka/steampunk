﻿using System.Collections;
using UnityEngine;

// Enemy (droid) attacker scropt
public class Attacker : MonoBehaviour
{
    public Animator fireAnim;
    public bool engaged = false;
    public GameObject shootFx;
    public GameObject bulletPrefab;
    public AudioClip shotSound;

    GameObject target;
    AudioSource src;

    public GameObject cannon1, cannon2;

    private void Start()
    {
        src = GetComponentInChildren<AudioSource>();        
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
            Engage(other.gameObject);
    }

    public void Engage(GameObject player)
    {
       // start shooting bullets at the player

        // TODO: optionally, check with raycast there are no obstructions,
        // so it can't shoot thru walls?
        // ...

    }

    IEnumerator FireBurst()
    {
        while (engaged)
        {
            if (src != null && shotSound != null)
                src.PlayOneShot(shotSound);

            for (int i = 0; i < 5; i++) {
                Fire();
                yield return new WaitForSeconds(0.3f);
            }
            // play screechy reload sound?
            yield return new WaitForSeconds(1f);
        }
    }

    void Fire()
    {
        if(target == null)
        {
            engaged = false;
            return;
        }

        Vector3 relativePos = target.transform.position - transform.position;
        transform.rotation = Quaternion.LookRotation(relativePos);

        if (shootFx != null)
        {
            Instantiate(shootFx, cannon1.transform.position, Quaternion.identity);
            Instantiate(shootFx, cannon2.transform.position, Quaternion.identity);
        }

        GameObject bullet = Instantiate(bulletPrefab, transform.position, Quaternion.identity);
        // it may miss due to bullets falling down with gravity
        bullet.GetComponent<Rigidbody>().AddForce(transform.forward);
    }
}
